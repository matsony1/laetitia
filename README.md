# Laetitia

![schild](/src/assets/images/schild-1.gif)

laetitia-gent.be

##
Via deze repo kan je de webpagina van Laetitia bewerken, 
althans mits pull request.

### Links om de site aan te passen
[Welkom](/src/app/pages/home/home.component.html)  
[Activiteiten](/src/app/pages/activiteiten/activiteiten.component.html)  
[Proseniores](/src/app/pages/proseniores/proseniores.component.html)  
[Praesidium](/src/app/pages/praesidium/praesidium.component.html)  
[Geschiedenis](/src/app/pages/geschiedenis/geschiedenis.component.html)  
[Schild](/src/app/pages/schild/schild.component.html)  
[Statuten](/src/app/pages/statuten/statuten.component.html)  
[Clublied](/src/app/pages/clublied/clublied.component.html)  

