import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ActiviteitenComponent } from './pages/activiteiten/activiteiten.component';
import { ProsenioresComponent } from './pages/proseniores/proseniores.component';
import { PraesidiumComponent } from './pages/praesidium/praesidium.component';
import { GeschiedenisComponent } from './pages/geschiedenis/geschiedenis.component';
import { SchildComponent } from './pages/schild/schild.component';
import { ClubliedComponent } from './pages/clublied/clublied.component';
import { StatutenComponent } from './pages/statuten/statuten.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'activiteiten', component: ActiviteitenComponent},
  {path: 'proseniores', component: ProsenioresComponent},
  {path: 'praesidium', component: PraesidiumComponent},
  {path: 'geschiedenis', component: GeschiedenisComponent},
  {path: 'schild', component: SchildComponent},
  {path: 'statuten', component: StatutenComponent},
  {path: 'clublied', component: ClubliedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
