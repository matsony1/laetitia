import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ActiviteitenComponent } from './pages/activiteiten/activiteiten.component';
import { ProsenioresComponent } from './pages/proseniores/proseniores.component';
import { PraesidiumComponent } from './pages/praesidium/praesidium.component';
import { GeschiedenisComponent } from './pages/geschiedenis/geschiedenis.component';
import { SchildComponent } from './pages/schild/schild.component';
import { StatutenComponent } from './pages/statuten/statuten.component';
import { ClubliedComponent } from './pages/clublied/clublied.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ActiviteitenComponent,
    ProsenioresComponent,
    PraesidiumComponent,
    GeschiedenisComponent,
    SchildComponent,
    StatutenComponent,
    ClubliedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
